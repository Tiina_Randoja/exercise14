﻿using System;

namespace calc
{
    class Program
    {
        static void Main(string[] args)
        {//Start the program with Clear();
        Console.Clear();
        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine("Hi. Please enter your month of birth as a number");
        var mob = int.Parse(Console.ReadLine());

        Console.WriteLine($"So you were born in {mob}st month of the year");
    
        Console.WriteLine("What about the date?");
        var dob = int.Parse(Console.ReadLine());
        Console.WriteLine("And then the year?");
        var yob = int.Parse(Console.ReadLine());
        Console.WriteLine ($"Wonderful.. so the date of your birthday is {dob} . {mob} . {yob} Press <enter> to exit");
        Console.ReadKey();
            
        }
    }
}